import React from 'react';
import Operation from './Operation';
import { Link } from "react-router-dom";

const AccountActivity = (props) => {
  const { activity, index } = props;
  const [opName, opObj] = activity.op;
  return (
    <tr>
      <td>{index}</td>
      <td>{activity.timestamp}</td>
      <td><Link to={`/block/${activity.block}`}>{activity.block}</Link></td>
      <td>
        <table>
          <tbody>
            <Operation opName={opName} opObj={opObj} />
          </tbody>
        </table>
      </td>
    </tr>
  );
};

export default AccountActivity;
