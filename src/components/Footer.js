import React from 'react';

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <ul className="menu-bottom">
          <li><a href="/">Home</a></li>
          <li><a href="https://whaleshares.io/">Social App</a></li>
          <li><a href="https://wallet.whaleshares.io/">Wallet</a></li>
          <li><a href="https://gitlab.com/beyondbitcoin/whaleshares-chain/wikis/uploads/71010b5a2493e95f343b0c7de1da5fce/Whalepaper_v1.pdf">WhalePaper</a></li>
          <li><a href="https://discord.gg/3pqBXKY">Discord</a></li>
        </ul>
      </footer>
    )
  }
}

export default Footer;