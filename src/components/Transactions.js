import React from 'react';
import Transaction from './Transaction';

const Transactions = (props) => {
  const { transactions } = props;

  return (
    <div className="BlockInfo__tab Transactions">
      { transactions.length ? (
        <table className="table table-hover">
          <tbody>
          {
            transactions.map((item, idx) => {
              return (<Transaction key={`tx_${idx}`} transaction={item} index={idx} />);
            })
          }
          </tbody>
        </table>
      ) : (      
        <div className="wls-message">
          No transactions in this block.
        </div>
      )}
    </div>
  );
};

export default Transactions;