import React from 'react';
import { Tabs } from 'antd';
import Operations from './Operations';
import Transactions from './Transactions';
import { Link } from "react-router-dom";
import UserAvatar from "./UserAvatar";
import { Descriptions } from 'antd';
import ReactJson from 'react-json-view'
const { TabPane } = Tabs;

const Block = (props) => {
  const { block, block_num } = props;

  let operationsCount = 0;
  let operations = [];
  block.transactions.forEach((tx) => {
    operationsCount += tx.operations.length;
    operations.push(...tx.operations);
  });

  return (
    <div className="BlockInfo">
      <div className="BlockInfo__descriptions">
        <Descriptions title="Block Info">
          <Descriptions.Item label="Height">{block_num}</Descriptions.Item>
          <Descriptions.Item label="Time">{block.timestamp}</Descriptions.Item>
          <Descriptions.Item label="Witness">
            <Link to={`/account/${block.witness}`}>
              <UserAvatar username={block.witness} size={32} />
            </Link>
          </Descriptions.Item>
          <Descriptions.Item label="Transactions">{block.transactions.length}</Descriptions.Item>
          <Descriptions.Item label="Operations">{operationsCount}</Descriptions.Item>
        </Descriptions>
      </div>
      <div className="BlockInfo__operations">
        <Tabs defaultActiveKey="operations" type="card">
          <TabPane tab="Operations" key="operations">
            <Operations operations={operations} />
          </TabPane>
          <TabPane tab="Transactions" key="transactions">
            <Transactions transactions={block.transactions} />
          </TabPane>
          <TabPane tab="Json" key="json">
            <div className="BlockInfo__tab json">
              <ReactJson
                src={block}
                collapseStringsAfterLength={32}
                enableClipboard={false}
                displayObjectSize={false}
                displayDataTypes={false}
              />
            </div>
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};

export default Block;
