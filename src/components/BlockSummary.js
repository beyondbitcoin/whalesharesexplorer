import React from 'react';
import { Link } from "react-router-dom";
import UserAvatar from './UserAvatar';

const BlockSummary = (props) => {
  const { operationsCount, block_num, block } = props;

  return (
    <tr key={`block_${block_num}`}>
      <td><Link to={`/block/${block_num}`}>{block_num}</Link></td>
      <td>{block.timestamp}</td>
      <td>
        <Link to={`/account/${block.witness}`}>
          <UserAvatar username={block.witness} size={32} />
        </Link>
      </td>
      <td>{block.transactions.length}</td>
      <td>{operationsCount}</td>
    </tr>
  );
};

export default BlockSummary;
