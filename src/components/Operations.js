import React from 'react';
import Operation from './Operation';

const Operations = (props) => {
  const { operations } = props;

  return (
    <div className="BlockInfo__tab Operations">
      { operations.length ? (
        <table className="table table-hover">
          <tbody>
            { operations.map((item, idx) => {
                const [opName, opObj] = item;
                return (<Operation key={`op_${idx}`} opName={opName} opObj={opObj} />);
            })}
          </tbody>
        </table>
      ) : (      
        <div className="wls-message">
          No operations in this block.
        </div>
      )}
    </div>
  );
};

export default Operations;