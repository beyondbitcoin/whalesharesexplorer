import React from 'react';
import BlockSummary from './BlockSummary';

const RecentBlocks = (props) => {
  const { blocks } = props;

  return (
    <table className="table table-hover">
      <thead>
      <tr className="table-primary">
        <th scope="col">Height</th>
        <th scope="col">Time</th>
        <th scope="col">Witness</th>
        <th scope="col" title="Transactions">Txs</th>
        <th scope="col" title="Operations">Ops</th>
      </tr>
      </thead>
      <tbody>
      {
        blocks.map((item) => {
          const [block_num, block] = item;
          let operationsCount = 0;
          block.transactions.forEach((tx) => {
            operationsCount += tx.operations.length;
          });

          return (<BlockSummary key={`block_sum_${block_num}`} block={block} block_num={block_num} operationsCount={operationsCount} />);
        })
      }
      </tbody>
    </table>
  );
};

export default RecentBlocks;
