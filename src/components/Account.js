import React from 'react';
import JsonTable from '../components/JsonTable';

const Account = (props) => {
  const { account } = props;

  return (
    <JsonTable json={account}/>
  );
};

export default Account;
