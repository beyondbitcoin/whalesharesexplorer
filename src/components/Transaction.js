import React from 'react';
import JsonTable from './JsonTable';

const Transaction = (props) => {
  const { transaction, index } = props;

  return (
    <tr>
      <td><b>{index}</b></td>
      <td>
        <JsonTable json={transaction}/>
      </td>
    </tr>
  );
};

export default Transaction;