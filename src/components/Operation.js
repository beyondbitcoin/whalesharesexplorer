import React from 'react';
import JsonTable from './JsonTable';

const Operation = (props) => {
  const { opName, opObj } = props;

  return (
    <tr className="op-item">
      <td className="op-name"><b>{opName}</b></td>
      <td className="op-obj">
        <JsonTable json={opObj}/>
      </td>
    </tr>
  );
};

export default Operation;