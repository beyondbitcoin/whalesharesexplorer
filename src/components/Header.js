import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faCubes } from "@fortawesome/pro-light-svg-icons";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      block_num: '',
    }
  };

  handleChange = (event) => {
    const target = event.target;
    const name = target.name;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    if (name === "username") value = value.toLowerCase();
    this.setState({[name]: value});
  };

  handleSubmitBlock = (event) => {
    event.preventDefault();
    // console.log(`handleSubmitBlock: block_num=${this.state.block_num}`);
    this.props.history.push(`/block/${this.state.block_num}`);
  };

  handleSubmitAccount = (event) => {
    event.preventDefault();
    // console.log(`handleSubmitAccount: username=${this.state.username}`);
    this.props.history.push(`/account/${this.state.username}`);
  };

  render() {
    return (
      <header className="Topnav">
        <div className="navbar navbar-dark">
          <a className="brand" href="###">
            <img src="/assets/img/wls-logo-beta.png" className="logo" alt="Whaleshares Logo" />
            <img src="/assets/img/wls-mark-beta.png" className="tmark" alt="Whaleshares Logo" />
            <span className="sitename">Explorer</span>
          </a>
          <nav className="top-menu">
            <Link to='/blocks_missed' style={{lineHeight: '32px', color: 'white'}}>[ Blocks Missed ]</Link>
            <form className="form-inline my-2 my-lg-0" onSubmit={this.handleSubmitBlock}>
              <div className="input-group">
                <input className="form-control form-control-sm" name="block_num" type="search" 
                      placeholder="Block #" aria-label="Block number" required=""
                      value={this.state.block_num} onChange={this.handleChange}
                />
                <div className="input-group-append">
                  <button className="btn btn-dark btn-sm" type="submit"><FontAwesomeIcon icon={faCubes} /> <span>Get</span></button>
                </div>
              </div>
            </form>
            <form className="form-inline my-2 my-lg-0" onSubmit={this.handleSubmitAccount}>
              <div className="input-group">
                <input className="form-control form-control-sm" name="username" type="search" placeholder="Account name"
                      aria-label="Account username" required=""
                      value={this.state.username} onChange={this.handleChange}
                />
                <div className="input-group-append">
                  <button className="btn btn-dark btn-sm" type="submit"><FontAwesomeIcon icon={faUser} /> <span>Get</span></button>
                </div>
              </div>
            </form>
          </nav>
        </div>
      </header>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  };
};

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withRouter, withConnect)(Header);
