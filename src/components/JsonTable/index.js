import React from 'react';
import JsonToHtml from './JsonToHtml';

class Index extends React.Component {
  render() {
    const htmlTable = JsonToHtml.getTable(this.props.json);

    return (
      <span dangerouslySetInnerHTML={{__html: htmlTable}}></span>
    );
  }
}

export default Index;
