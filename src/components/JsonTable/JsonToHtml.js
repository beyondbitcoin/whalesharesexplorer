import Css from './Css';

const JsonToHtml = (function() {
  let html = '';
  let level = 0;
  let rootClass = "";
  let suffix = '';
  // let colspan = 2;
  let jsonObjOrig;
  let subLevel = 0;
  let componentLevel = 0;

  let getTable = function(jsonObj) {
    html = '<table  class="table table-hover">';
    jsonObjOrig = jsonObj;
    level = 0;
    walkTheDog(jsonObj);
    html += '</table>';
    return html;
  };

  let getIndent = function(level) {
    let indent = '';

    for (let i=0; i<level; i++) {
      indent += '';
    }

    return indent;
  };

  // Get the Css obj from Css.js, and return a semicolon separated list of styles
  let getStyleAttributes = function(className) {
    let cssObj = Css[className];
    let keys = Object.keys(cssObj);
    let attributes = "";

    for(let i=0; i<keys.length; i++) {
      let key = keys[i];
      let cssAttr = key.replace(/([A-Z])/g, "-$1").toLowerCase();
      attributes += cssAttr + ":" + cssObj[key] + ";";
    }

    return attributes;
  };

  let processArray = function(arr) {
    let html = '';

    if (Array.isArray(arr) && arr.length === 0) {
      return html;
    }

    for (let k in arr) {
      let v = arr[k];
      if (typeof v === 'object') {
        const htmlTable = JsonToHtml.getTable(v);

        html += '<tr class="json tblrow v1">';
        html += '<td></td><td class="json tblcol v1" style="' + getStyleAttributes('dataCell') + '">' + htmlTable + '</td>';
        html += '</tr>';
      } else {
        html += '<tr class="json tblrow v2">';
        html += '<td></td>'
        html += '  <td class="json tblcol v2" style="' + getStyleAttributes('dataCell') + '">' + getIndent(level) + v + suffix + '</td>';
        html += '</tr>';
      }
    }

    return html;
  };

  let walkTheDog = function(jsonObj) {
    let hasArray = false;

    if (typeof jsonObj === 'string') {
      jsonObj = JSON.parse(jsonObj);
    }

    subLevel = level;

    for (let k in jsonObj) {
      // Reset the indent if next element is root
      if (typeof jsonObjOrig[k] !== 'undefined') {
        level = 0;
        rootClass = getStyleAttributes('rootElement');
      }
      else {
        rootClass = getStyleAttributes('subElement');
      }

      componentLevel = subLevel;

      if (jsonObj.hasOwnProperty(k)) {
        let v = jsonObj[k];

        if (hasArray) {
          level = componentLevel;
        }

        if (typeof v === 'object') {
          // colspan += level;
          html += '<tr class="json tblrow v3"><td class="json tblcol v3" colspan="2" style="' + rootClass+ '">' + getIndent(level) + k + suffix + '</td></tr>';
          level += 1;
        }
        else {
          let style = getStyleAttributes('jsonTd') + getStyleAttributes('dataCell')
          html += '<tr class="json tblrow v4"><td class="json tblcol v4" style="' + rootClass + '">' + getIndent(level) + k + suffix + '</td><td style="' + style + '">' + v + '</td></tr>';
        }

        if (v instanceof Array) {
          html += processArray(v);
          hasArray = true;
        }

        if (typeof v === 'object' && !(v instanceof Array)) {
          walkTheDog(v);
          level = subLevel - 1; // Outdent back
        }
      }
    }
  };

  return {
    getTable: getTable
  }

})();

export default JsonToHtml;
