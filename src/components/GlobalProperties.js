import React from 'react';

const GlobalProperties = (props) => (
  <table className="table table-hover">
    <thead>
    <tr className="table-primary">
      <th>Global Properties</th>
      <th>Value</th>
    </tr>
    </thead>
    <tbody>
    {
      Object.keys(props).map((key) => (
        <tr key={key}>
          <td className="table-field">{key}</td>
          <td><b data-prop="time">{props[ key ]}</b></td>
        </tr>
      ))
    }
    </tbody>
  </table>
);

export default GlobalProperties;
