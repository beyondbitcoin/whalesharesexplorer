import React from 'react';
import AccountActivity from './AccountActivity';
import { Button } from 'antd';

const AccountActivites = (props) => {
  const { activities, loadMore } = props;
  const { loading, hasMore, list } = activities;

  // if (loading) return (<div>Loading...</div>);

  return (
    <div>
      <table className="table table-hover">
        <thead>
        <tr className="table-primary">
          <th scope="col"></th>
          <th scope="col">Time</th>
          <th scope="col">Block</th>
          <th scope="col">Operation</th>
        </tr>
        </thead>
        <tbody>
        {
          list.map((item) => {
            const [ idx, obj ] = item;
            return (<AccountActivity key={`acc_activity_${idx}`} activity={obj} index={idx} />);
          })
        }
        </tbody>
      </table>

      <div className="text-center">
        {
          loading ? <Button shape="circle" loading /> : hasMore ? <Button type="primary" onClick={() => loadMore()}>Load More...</Button> : <div>End, nothing more to load...</div>
        }
      </div>
    </div>
  );
};

export default AccountActivites;
