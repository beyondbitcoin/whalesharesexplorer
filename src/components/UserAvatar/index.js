import React from 'react';
import PropTypes from 'prop-types';
import { Avatar } from 'antd';

export function getAvatarURL(username, size = 32) {
  let wxh = 128;
  if (size <= 32) {
    wxh = 32;
  } else if (size <= 48) {
    wxh = 48;
  } else if (size <= 64) {
    wxh = 64;
  } else if (size <= 96) {
    wxh = 96;
  } else {
    wxh = 128;
  }

  return `https://imgp.whaleshares.io/profileimage/${username}/${wxh}x${wxh}`;
}

const UserAvatar = ({ username, size }) => {
  // let style = {
  //   minWidth: `${size}px`,
  //   width: `${size}px`,
  //   height: `${size}px`,
  // };

  const url = getAvatarURL(username, size);
  return (
    <div>
      <Avatar src={url}/>
      <span style={{ paddingLeft: 5 }}>
        {username}
      </span>
    </div>
  );
};

UserAvatar.propTypes = {
  username: PropTypes.string.isRequired,
  size: PropTypes.number,
};

UserAvatar.defaultProps = {
  size: 32,
};

export default UserAvatar;
