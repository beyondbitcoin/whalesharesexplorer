import { WLS_RPC_URL } from './settings';

const getJsonRpcUri = async (api, method, params) => {
  const encodedParams = encodeURIComponent(btoa(JSON.stringify(params)));
  const url = `${WLS_RPC_URL}/${api}/${method}/${encodedParams}`;
  const api_res = await fetch(url);
  const api_res_json = await api_res.json();
  return api_res_json['result'];
};

export default getJsonRpcUri;
