import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
// import injectSaga from '../../utils/injectSaga';
// import injectReducer from '../../utils/injectReducer';
import { makeSelectApp } from './selectors';
// import {setIn} from '../../actions';
// import reducer from './reducer';
// import saga from './saga';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Home from '../../containers/Home'
import Block from '../Block';
import Account from '../Account';
import BlocksMissed from '../BlocksMissed';
import './App.css';

export class Index extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <main className="Main" role="main">
          <Switch>
            <Route path='/blocks_missed' component={BlocksMissed}/>
            <Route path='/block/:block_num' component={Block}/>
            <Route path='/account/:account_name' component={Account}/>
            <Route component={Home}/>
          </Switch>
        </main>
        <Footer/>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
});
const mapDispatchToProps = dispatch => ({});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
// const withReducer = injectReducer({ key: 'app', reducer });
// const withSaga = injectSaga({ key: 'app', saga });

export default compose(
  // withReducer,
  // withSaga,
  withRouter,
  withConnect
)(Index);
