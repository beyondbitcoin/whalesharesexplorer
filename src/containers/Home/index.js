import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPause, faPlay } from "@fortawesome/pro-light-svg-icons";
import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import { makeSelectHome } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { actionMonitorStart, actionMonitorStop } from './actions';
import GlobalProperties from "../../components/GlobalProperties";
import RecentBlocks from '../../components/RecentBlocks';

class Index extends Component {

  componentDidMount() {
    this.props.monitorStart();
  }

  render() {
    const { recent_blocks, dynamic_global_properties } = this.props;
    const { head_block_number, last_irreversible_block_num } = dynamic_global_properties;
    const revers_blocks_count = head_block_number ? head_block_number - last_irreversible_block_num : 0;

    return (
      <div className="Home">
        <div className="Params">
          <div className="params-title">
            <div>
              <h1>Recent Blockchain Activity</h1>
            </div>
            <div>
              {this.props.monitoring ? (
                <button type="button" className="btn btn-secondary btn-dark btn-sm float-right monitor" onClick={() => this.props.monitorStop()}>
                  <FontAwesomeIcon icon={faPause} /> <span>Pause</span>
                </button>
                ) : (
                <button type="button" className="btn btn-secondary btn-dark btn-sm float-right monitor" onClick={() => this.props.monitorStart()}>
                  <FontAwesomeIcon icon={faPlay} /> <span>Resume</span>
                </button>
              )}
            </div>
          </div>
          <div className="params-content">
            <div>Current Height: <span className="badge badge-primary">{head_block_number}</span></div>
            <div>Reversible blocks awaiting concensus: <span className="badge badge-warning">{revers_blocks_count}</span></div>
          </div>
        </div>
        <div className="Params">
          <div className="Activity">
            <RecentBlocks blocks={recent_blocks} />
          </div>
          <div className="Properties">
            <GlobalProperties {...this.props.dynamic_global_properties} />
          </div>
        </div>
      </div>
    );
  }
}

// createStructuredSelector
// const mapStateToProps = createStructuredSelector({
//  makeSelectHome()
// });
const mapStateToProps = makeSelectHome();
const mapDispatchToProps = dispatch => ({
  monitorStart: () => dispatch(actionMonitorStart()),
  monitorStop: () => dispatch(actionMonitorStop()),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Index);
