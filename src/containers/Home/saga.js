import { delay, put, select, takeLatest } from 'redux-saga/effects';
import { makeSelectHome } from './selectors';
import { ACTION_MONITOR_START } from './constants';
import { actionUpdate } from './actions';
import getJsonRpcUri from '../../utils/JsonRpcUri';

export function* monitorStartTask() {
  while (true) {
    try {
      const homeState = yield select(makeSelectHome());
      const { monitoring, block, recent_blocks, dynamic_global_properties } = homeState;
      if (!monitoring) {
        break;
      }
      const dgp =  yield getJsonRpcUri("database_api", "get_dynamic_global_properties", []);
      yield put(actionUpdate('dynamic_global_properties', dgp));

      if (block === 0) {
        let block = dgp.head_block_number;
        yield put(actionUpdate('block', block));

        const block_obj = yield getJsonRpcUri("database_api", "get_block", [block]);
        yield put(actionUpdate('recent_blocks', [[block, block_obj]]));

        yield delay(4000);
        continue;
      } else {
        let new_recent_blocks = recent_blocks;
        for (let i = block + 1; i<=dynamic_global_properties.head_block_number; i++) {
          yield put(actionUpdate('block', i));
          const block_obj = yield getJsonRpcUri("database_api", "get_block", [i]);
          new_recent_blocks = [[i, block_obj], ...new_recent_blocks];
          new_recent_blocks = new_recent_blocks.slice(0, 20);
          yield put(actionUpdate('recent_blocks', new_recent_blocks));

          yield delay(1000);
        }
      }
    } catch (e) {
      // do nothing
    } finally {
      yield delay(6000);
    }
  }
}


export default function* homeSaga() {
  yield takeLatest(ACTION_MONITOR_START, monitorStartTask);
}
