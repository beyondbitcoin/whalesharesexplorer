import {
  ACTION_UPDATE,
  ACTION_LOAD_BLOCK
} from './constants';

export const actionUpdate = (name, value) => ({ type: ACTION_UPDATE, payload: {name, value} });
export const actionLoadBlock = (block_num) => ({ type: ACTION_LOAD_BLOCK, payload: {block_num} });
