import _ from "lodash";
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectBlockState = state => _.get(state, 'block', initialState);

const makeSelect = key => createSelector(selectBlockState, state => _.get(state, key));

const makeSelectBlock = () => createSelector(selectBlockState, substate => substate);
export { makeSelectBlock, makeSelect };
