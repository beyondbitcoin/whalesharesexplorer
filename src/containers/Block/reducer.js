import produce from 'immer';
import { ACTION_UPDATE } from "./constants";

export const initialState = {
  block_num: 0,
  block: false,
  loading: false,
};

const blockReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ACTION_UPDATE:
        draft[ action.payload.name ] = action.payload.value;
        break;
      default:
        break;
    }
  });

export default blockReducer;
