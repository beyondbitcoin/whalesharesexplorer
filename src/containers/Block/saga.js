import { put, takeLatest } from 'redux-saga/effects';
import { ACTION_LOAD_BLOCK } from './constants';
import { actionUpdate } from "./actions";
import getJsonRpcUri from "../../utils/JsonRpcUri";

export function* loadBlockTask(action) {
  const {block_num} = action.payload;
  try {
    // console.log(`loadBlockTask: block_num=${block_num}`);
    yield put(actionUpdate('loading', true));

    const block_obj = yield getJsonRpcUri("database_api", "get_block", [block_num]);
    yield put(actionUpdate('block', block_obj));
    yield put(actionUpdate('block_num', block_num));
  } catch (e) {
    //--
  } finally {
    yield put(actionUpdate('loading', false));
  }
}


export default function* blockSaga() {
  yield takeLatest(ACTION_LOAD_BLOCK, loadBlockTask);
}
