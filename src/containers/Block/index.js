import React, { Component } from 'react';
import { matchPath, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import { makeSelectBlock } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { actionLoadBlock } from './actions';
import Block from '../../components/Block';
import { Button } from 'antd';

class Index extends Component {
  constructor(props) {
    super(props);

    this.props.history.listen((location, action) => {
      const match = matchPath(location.pathname, {
        path: '/block/:block_num',
        exact: false,
        strict: false
      });

      if (match) {
        const { block_num } = match.params;
        const props_block_num = this.props.block_num;
        // console.log(`history.listen: location=${JSON.stringify(location)}, action=${action}, account_name=${account_name}, account=${account.name}`);

        if (block_num !== props_block_num) {
          this.props.loadBlock(block_num);
        }
      }
    });
  };

  componentDidMount() {
    const { block_num } = this.props.match.params;
    if (block_num) this.props.loadBlock(block_num);
  }

  render() {
    const { block_num } = this.props.match.params;
    const { block, loading } = this.props;

    if (loading || (!block)) return (
      <div className="text-center p-3">
        <Button shape="circle" loading />
      </div>
    );

    return (
      <Block block_num={block_num} block={block} />
    );
  }
}

const mapStateToProps = makeSelectBlock();
const mapDispatchToProps = dispatch => ({
  loadBlock: (block_num) => dispatch(actionLoadBlock(block_num)),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'block', reducer });
const withSaga = injectSaga({ key: 'block', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Index);
