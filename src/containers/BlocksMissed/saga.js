import _ from 'lodash';
import { put, select, takeLatest } from 'redux-saga/effects';
import { message } from "antd";
import { ACTION_LOAD, ACTION_LOAD_MORE } from "./reducer";
import { actionUpdate } from "./reducer";
import { makeSelectBlocksMissed } from './reducer';
import getJsonRpcUri from "../../utils/JsonRpcUri";

export function* loadWorker(action) {
  try {
    yield put(actionUpdate(['loading'], true));

    const blocks_missed = yield getJsonRpcUri("database_api", "get_blocks_missed", [-1, 20]);
    yield put(actionUpdate(['list'], blocks_missed));
    let hasMore = false;
    if (blocks_missed.length >= 20) hasMore = true;
    yield put(actionUpdate(['hasMore'], hasMore));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(actionUpdate(['loading'], false));
  }
}

export function* loadMoreWorker() {
  try {
    yield put(actionUpdate(['loading'], true));

    const blocksMissedState = yield select(makeSelectBlocksMissed());
    let {list} = blocksMissedState;
    const lastIndex = list[list.length - 1][0];

    const blocks_missed = yield getJsonRpcUri("database_api", "get_blocks_missed", [lastIndex - 1, 20]);
    list = _.concat(list, blocks_missed);
    yield put(actionUpdate(['list'], list));
    let hasMore = false;
    if (blocks_missed.length >= 20) hasMore = true;
    yield put(actionUpdate(['hasMore'], hasMore));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(actionUpdate(['loading'], false));
  }
}

export default function* blocksMissedSaga() {
  yield takeLatest(ACTION_LOAD, loadWorker);
  yield takeLatest(ACTION_LOAD_MORE, loadMoreWorker);
}
