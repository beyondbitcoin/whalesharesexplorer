import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import { Button } from 'antd';
import reducer from './reducer';
import { actionLoad, actionLoadMore } from './reducer';
import { makeSelectBlocksMissed } from './reducer';
import saga from './saga';

const BlockMissedRow = (props) => {
  const {owner, block_num, timestamp} = props;
  return (
    <tr>
      <td>{owner}</td>
      <td>{timestamp}</td>
      <td>{block_num}</td>
    </tr>
  );
};

class Index extends Component {
  componentDidMount() {
    const {loading, list, load} = this.props;
    if ((loading === false ) && (list === false)) {
      load();
    }
  }

  render() {
    const { loading, hasMore, list, loadMore } = this.props;

    if (list === false) return (<div>Loading...</div>);

    return (
      <div className="BlocksMissed">
        <h5>Recent Blocks Missed</h5>
        <table className="table table-hover">
          <thead>
          <tr className="table-primary">
            <th scope="col">Witness</th>
            <th scope="col">Time</th>
            <th scope="col">Block</th>
          </tr>
          </thead>
          <tbody>
          {
            list.map((item) => {
              const [idx, obj] = item;
              return (<BlockMissedRow key={`block_missed_${idx}`} index={idx} {...obj} />);
            })
          }
          </tbody>
        </table>

        <div className="text-center">
          {
            loading ? <Button shape="circle" loading/> : hasMore ?
              <Button type="primary" onClick={() => loadMore()}>Load More...</Button> :
              <div>End, nothing more to load...</div>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = makeSelectBlocksMissed();
const mapDispatchToProps = dispatch => ({
  load: () => dispatch(actionLoad()),
  loadMore: () => dispatch(actionLoadMore()),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'blocksMissed', reducer });
const withSaga = injectSaga({ key: 'blocksMissed', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Index);
