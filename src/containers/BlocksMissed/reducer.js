import produce from 'immer';
import _ from "lodash";
import { createSelector } from 'reselect';

/** constants */
export const ACTION_UPDATE = 'blocksMissed/ACTION_UPDATE';
export const ACTION_LOAD = 'blocksMissed/ACTION_LOAD';
export const ACTION_LOAD_MORE = 'blocksMissed/ACTION_LOAD_MORE';

export const initialState = {
  loading: false,
  hasMore: false,
  list: false, /** array, false on init */
};

const blocksMissedReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ACTION_UPDATE:
        _.set(draft, action.payload.name, action.payload.value);
        break;
      default:
        break;
    }
  });

export default blocksMissedReducer;

/** actions */
export const actionUpdate = (name, value) => ({ type: ACTION_UPDATE, payload: {name, value} });
export const actionLoad = () => ({ type: ACTION_LOAD, payload: {} });
export const actionLoadMore = () => ({ type: ACTION_LOAD_MORE, payload: {} });

/** reselect */
export const selectBlocksMissedState = state => _.get(state, 'blocksMissed', initialState);
export const makeSelect = key => createSelector(selectBlocksMissedState, state => _.get(state, key));
export const makeSelectBlocksMissed = () => createSelector(selectBlocksMissedState, substate => substate);

