import produce from 'immer';
import _ from "lodash";
import { ACTION_UPDATE } from "./constants";

export const initialState = {
  account: false,
  loading: false,
  activities: {
    loading: false,
    hasMore: false,
    // lastIndex: 0,
    list: [],
  }
};

const accountReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ACTION_UPDATE:
        _.set(draft, action.payload.name, action.payload.value);
        break;
      default:
        break;
    }
  });

export default accountReducer;
