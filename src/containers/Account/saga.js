import _ from 'lodash';
import { put, select, takeLatest } from 'redux-saga/effects';
import { message } from "antd";
import { ACTION_LOAD_ACCOUNT, ACTION_LOAD_ACTIVITIES, ACTION_LOAD_MORE_ACTIVITIES } from "./constants";
import { actionUpdate } from "./actions";
import getJsonRpcUri from "../../utils/JsonRpcUri";
import { makeSelectAccount } from "./selectors";

export function* loadAccountTask(action) {
  const { account_name } = action.payload;
  try {
    // console.log(`loadAccountTask: account_name=${account_name}`);
    yield put(actionUpdate('loading', true));

    const [ acc_obj ] = yield getJsonRpcUri("database_api", "get_accounts", [ [ account_name ] ]);
    yield put(actionUpdate('account', acc_obj));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(actionUpdate('loading', false));
  }
}

export function* loadAccountActivitiesTask(action) {
  const { account_name } = action.payload;
  try {
    // console.log(`loadAccountActivitiesTask: account_name=${account_name}`);
    yield put(actionUpdate([ 'activities', 'loading' ], true));

    const acc_hist = yield getJsonRpcUri("database_api", "get_account_history", [ account_name, -1, 20 ]);
    yield put(actionUpdate([ 'activities', 'list' ], acc_hist));
    let hasMore = false;
    if (acc_hist.length >= 20) hasMore = true;
    yield put(actionUpdate([ 'activities', 'hasMore' ], hasMore));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(actionUpdate([ 'activities', 'loading' ], false));
  }
}

export function* loadMoreAccountActivitiesTask() {
  try {
    yield put(actionUpdate([ 'activities', 'loading' ], true));

    const accountState = yield select(makeSelectAccount());
    const { account, activities } = accountState;
    const account_name = account.name;
    let { list } = activities;
    const lastIndex = list[ list.length - 1 ][ 0 ];
    // console.log(`loadMoreAccountActivitiesTask: lastIndex=${lastIndex}`);

    const acc_hist = yield getJsonRpcUri("database_api", "get_account_history", [ account_name, lastIndex - 1, 20 ]);
    list = _.concat(list, acc_hist);
    yield put(actionUpdate([ 'activities', 'list' ], list));
    let hasMore = false;
    if (acc_hist.length >= 20) hasMore = true;
    yield put(actionUpdate([ 'activities', 'hasMore' ], hasMore));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(actionUpdate([ 'activities', 'loading' ], false));
  }
}

export default function* accountSaga() {
  yield takeLatest(ACTION_LOAD_ACCOUNT, loadAccountTask);
  yield takeLatest(ACTION_LOAD_ACTIVITIES, loadAccountActivitiesTask);
  yield takeLatest(ACTION_LOAD_MORE_ACTIVITIES, loadMoreAccountActivitiesTask);
}
