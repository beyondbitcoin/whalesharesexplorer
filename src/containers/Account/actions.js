import {
  ACTION_UPDATE,
  ACTION_LOAD_ACCOUNT,
  ACTION_LOAD_ACTIVITIES,
  ACTION_LOAD_MORE_ACTIVITIES
} from './constants';

export const actionUpdate = (name, value) => ({ type: ACTION_UPDATE, payload: {name, value} });
export const actionLoadAccount = (account_name) => ({ type: ACTION_LOAD_ACCOUNT, payload: {account_name} });
export const actionLoadAccountActivities = (account_name) => ({ type: ACTION_LOAD_ACTIVITIES, payload: {account_name} });
export const actionLoadMoreAccountActivities = () => ({ type: ACTION_LOAD_MORE_ACTIVITIES, payload: {} });
