import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import { makeSelectAccount } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { actionLoadAccount, actionLoadAccountActivities, actionLoadMoreAccountActivities } from './actions';
import Account from '../../components/Account';
import AccountActivites from "../../components/AccountActivites";
import { matchPath } from 'react-router'

class Index extends Component {
  constructor(props) {
    super(props);

    this.props.history.listen((location, action) => {
      const match = matchPath(location.pathname, {
        path: '/account/:account_name',
        exact: false,
        strict: false
      });

      if (match) {
        const { account_name } = match.params;
        const { account } = this.props;
        // console.log(`history.listen: location=${JSON.stringify(location)}, action=${action}, account_name=${account_name}, account=${account.name}`);

        if (account_name !== account.name) {
          this.props.loadAccount(account_name);
          this.props.loadAccountActivities(account_name);
        }
      }
    });
  };

  componentDidMount() {
    const { account_name } = this.props.match.params;
    if (account_name) {
      this.props.loadAccount(account_name);
      this.props.loadAccountActivities(account_name);
    }
  }

  render() {
    const { account_name } = this.props.match.params;
    const { account, loading, activities } = this.props;

    if (loading || (account === false)) return (<div>Loading...</div>);

    return (
      <div className="AccData">
        <div className="Recent">
          <div className="Activities">
            <h5>Recent Activities of @{account_name}</h5>
            <AccountActivites activities={activities} loadMore={this.props.loadMoreAccountActivities}/>
          </div>
          <div className="Account">
            <h5>Account</h5>
            <Account account={account}/>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = makeSelectAccount();
const mapDispatchToProps = dispatch => ({
  loadAccount: (account_name) => dispatch(actionLoadAccount(account_name)),
  loadAccountActivities: (account_name) => dispatch(actionLoadAccountActivities(account_name)),
  loadMoreAccountActivities: () => dispatch(actionLoadMoreAccountActivities()),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'account', reducer });
const withSaga = injectSaga({ key: 'account', saga });

export default compose(
  withRouter,
  withReducer,
  withSaga,
  withConnect
)(Index);
