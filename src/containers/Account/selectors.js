import _ from "lodash";
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAccountState = state => _.get(state, 'account', initialState);

const makeSelect = key => createSelector(selectAccountState, state => _.get(state, key));

const makeSelectAccount = () => createSelector(selectAccountState, substate => substate);
export { makeSelectAccount, makeSelect };
