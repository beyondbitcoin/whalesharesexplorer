# Whaleshares Explorer

[![pipeline status](https://gitlab.com/beyondbitcoin/whalesharesexplorer/badges/master/pipeline.svg)](https://gitlab.com/beyondbitcoin/whalesharesexplorer/commits/master)

![Preview](https://gitlab.com/beyondbitcoin/whalesharesexplorer/wikis/uploads/ee01906215be292c05dd3b38bf6676af/Screen_Shot_2019-06-21_at_7.26.58_PM.png)

### Getting Started

```
yarn
yarn start
```

Open `http://localhost:3000/`
